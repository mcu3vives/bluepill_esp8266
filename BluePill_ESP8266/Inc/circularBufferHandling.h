#include "stm32f1xx_hal.h"

#define SERIAL_RX_BUFFER_SIZE 512


#define CIRCBUF_DEF(x,y)          \
    uint8_t x##_dataSpace[y] = {{ 0 }};     \
    circBuf_t x = {               \
        .buffer = x##_dataSpace,      \
        .head = 0,                \
        .tail = 0,                \
        .maxLen = (y)               \
    }
	
		
typedef struct{
    uint8_t *const buffer;
    int head;
    int tail;
    const int maxLen;
} circBuf_t;

int circBufPush(uint8_t data);
int circBuffPop(uint8_t *data);
int circAvailable(void);
void circFlush(void);
