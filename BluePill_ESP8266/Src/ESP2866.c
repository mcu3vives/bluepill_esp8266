#include "ESP2866.h"
#include <string.h>
#include <stdbool.h>
#include "millis.h"
#include "circularBufferHandling.h"

/* Peripheral handles BEGIN */
UART_HandleTypeDef huart2;
/* Peripheral handles END */

/* private variables */
bool multiple_connections = false;
uint8_t rx_data = 0;
/* private variables END */

/* Buffer for creating C-string */
char string_buff[SERIAL_RX_BUFFER_SIZE];
/* Buffer for creating C-string END */





ESP_STATE moduleAvailable(const int timeout, bool debug){	
	char cmd[] = "AT\r\n";
	
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP

	return receiveHandling(timeout, debug);
}



ESP_STATE restartModule(const int timeout, bool debug){	
	char cmd[] = "AT+RST\r\n";

	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
}

ESP_STATE fetchFirmwareVersion(const int timeout, bool debug){
	char cmd[] = "AT+GMR\r\n";

	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE enterDeepsleep(int time_sleep, const int timeout, bool debug){	
	char cmd[100];
	
	sprintf(cmd,"AT+GSLP=%d\r\n",time_sleep);
	
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE toggleEcho(int mode, const int timeout, bool debug){	
	char cmd[100];
	
	sprintf(cmd,"ATE%d\r\n",mode);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE changeWifiMode(int mode, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CWMODE=%d\r\n",mode);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
}

ESP_STATE currentWifiMode(const int timeout, bool debug){
	char cmd[] = "AT+CWMODE?\r\n";

	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
	
}
	
ESP_STATE connectToAccessPoint(char* ssid, char* pwd, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CWJAP=\"%s\",\"%s\"\r\n",ssid, pwd);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentConnectedAccessPoint(const int timeout, bool debug){	
	char cmd[]="AT+CWJAP?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE listAvailableAccessPoints(int ecn, char* ssid, int rssi, char* mac, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CWLAP=%d,\"%s\",%d,\"%s\"\r\n",ecn,ssid,rssi,mac);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}


ESP_STATE disconnectFromAccessPoint(const int timeout, bool debug){
	char cmd[]="AT+CWQAP\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE configureSoftAPMode(char* ssid, char* pwd, int ch, int ecn, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CWSAP=\"%s\",\"%s\",%d,%d\r\n",ssid,pwd,ch,ecn);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentSoftAPMode(const int timeout, bool debug){
	char cmd[]="AT+CWSAP?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE listConnectedClients(const int timeout, bool debug){
	char cmd[]="AT+CWLIF\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}


ESP_STATE toggleDHCP(int mode, int en, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CWDHCP=%d,%d\r\n",mode,en);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
}

ESP_STATE setMACAddressStation(char* mac, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPSTAMAC=\"%s\"\r\n",mac);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentMACAddressStation(const int timeout, bool debug){
	char cmd[]="AT+CIPSTAMAC?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE setMACAddressSoftAP(char* mac, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPAPMAC=\"%s\"\r\n",mac);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
	
}

ESP_STATE currentMACAddressSoftAP(const int timeout, bool debug){
	char cmd[]="AT+CIPAPMAC?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE setIPAddressStation(char* ip, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPSTA=\"%s\"\r\n",ip);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentIPAddressStation(const int timeout, bool debug){
	char cmd[]="AT+CIPSTA?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE setIPAddressSoftAP(char* ip, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPAP=\"%s\"\r\n",ip);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentIPAddressSoftAP(const int timeout, bool debug){
	char cmd[]="AT+CIPAP?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}


ESP_STATE connectionInformation(int status, int id, char* type, char* addr, int port, int tetype, const int timeout, bool debug){
	char cmd[100];
	
	if(multiple_connections){
		sprintf(cmd,"AT+CIPSTATUS=%d,%d,\"%s\",\"%s\",%d,%d\r\n",status,id,type,addr,port, tetype);
	}
	else{
		sprintf(cmd,"AT+CIPSTATUS=%d,\"%s\",\"%s\",%d,%d\r\n",status,type,addr,port, tetype);
	}
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE connectionInformationSimple( const int timeout, bool debug){
	char cmd[] = "AT+CIPSTATUS\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE openConnection(int id, char* type, char* addr, int port, const int timeout, bool debug){	
	char cmd[100];
	
	if(multiple_connections){
		sprintf(cmd,"AT+CIPSTART=%d,\"%s\",\"%s\",%d\r\n",id,type,addr,port);
	}
	else{
		sprintf(cmd,"AT+CIPSTART=\"%s\",\"%s\",%d\r\n",type,addr,port);
	}
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE testConnection(const int timeout, bool debug){
	char cmd[]="AT+CIPSTART=?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}


ESP_STATE initSendData(int id, int len, const int timeout, bool debug){
	char cmd[100];

	if(multiple_connections){
		sprintf(cmd,"AT+CIPSEND=%d,%d\r\n",id,len);
	}
	else{
		sprintf(cmd,"AT+CIPSEND=%d\r\n",len);
	}	
	
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE testSendData(const int timeout, bool debug){
	char cmd[] = "AT+CIPSEND=?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE closeConnection(int id, const int timeout, bool debug){
	char cmd[100];

	if(multiple_connections){
		sprintf(cmd,"AT+CIPCLOSE=%d\r\n",id);
	}
	else{
		strcpy(cmd,"AT+CIPCLOSE\r\n");
	}
				
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE getLocalIPAddress(const int timeout, bool debug){
	char cmd[] = "AT+CIFSR\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);
}

ESP_STATE toggleMultipleConnection(int mode, const int timeout, bool debug){
	char cmd[100];
	

	if(mode == 1){
		multiple_connections = true;
	}
	else{
		multiple_connections = false;
	}

	sprintf(cmd,"AT+CIPMUX=%d\r\n",mode);
	
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentMultipleConnectionMode(const int timeout, bool debug){
	char cmd[] = "AT+CIPMUX?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE configureAsServer(int mode, int port, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPSERVER=%d,%d\r\n",mode,port);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE setTransferMode(int mode, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPMODE=%d\r\n",mode);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentTransferMode(const int timeout, bool debug){
	char cmd[] = "AT+CIPMODE?\r\n";
			
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE setServerTimeout(int time_level, const int timeout, bool debug){
	char cmd[100];
	
	sprintf(cmd,"AT+CIPSTO=%d\r\n",time_level);
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE currentServerTimeout(const int timeout, bool debug){	
	char cmd[] = "AT+CIPSTO?\r\n";
		
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)cmd, strlen(cmd) + 1); // Send read characters to ESP
	
	return receiveHandling(timeout, debug);

}

ESP_STATE receiveHandling(int timeout, bool debug){
	// Clear string buffer
	memset(string_buff, 0, sizeof(string_buff));
	
	int position = 0;

	int available = circAvailable();
	if(!available){
		HAL_UART_Receive_IT(&huart2, &rx_data, 1); // If no data available request some
	}
		
	long int time = millis();
	while((time + timeout) > millis()){
		while(circAvailable()){
				char c = readFromESP();
				string_buff[position++] = c;
		}
	}
	if(debug){
		printf("%s\r\n", string_buff);
	}
	
		
	/* check for "OK" */
	char* pch;
	pch = strstr(string_buff, "OK");
	
	if(pch)
		return ESP_OK;

	return ESP_NOK;
}

char* receiveHandlingAlt(int timeout, bool debug){
	// Clear string buffer
	memset(string_buff, 0, sizeof(string_buff));
	
	int position = 0;

	int available = circAvailable();
	if(!available){
		HAL_UART_Receive_IT(&huart2, &rx_data, 1); // If no data available request some
	}
		
	long int time = millis();
	while((time + timeout) > millis()){
		while(circAvailable()){
				char c = readFromESP();
				string_buff[position++] = c;
		}
	}
	return string_buff;
}


void sendData(char* msg){
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg, strlen(msg) + 1); 
}

char* readFromESPAsString(int timeout, bool debug){
	return receiveHandlingAlt(timeout, debug);
}

char readFromESP(void){
	uint8_t outData;
	
	for(int i = 0; i < SERIAL_RX_BUFFER_SIZE; i++){
				if(circBuffPop(&outData)){
						break;
				}
				return (char)outData;
		 }
	return 0;
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{  
	if(huart->Instance == USART2){
		circBufPush(rx_data);
		HAL_UART_Receive_IT(&huart2, &rx_data, 1); // Receive next character
	}
}		
