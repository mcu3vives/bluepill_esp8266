#include "stm32f1xx_hal.h"
#include "HM10.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>


UART_HandleTypeDef huart3;


void Transmit(char* msg);
HMState state = HM_NOK;

 
 /**
  * @brief  Query the native MAC address.
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Adress(void) {
Transmit("AT+ADDR?"); 
return state; 
}


/**
  * @brief  Query the baud rate
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Baud(void) {
Transmit("AT+BAUD?"); 
return state; 	
}


/**
  * @brief  Set the baud rate
  * @param  Baud rate
  * @retval Transmission state
  */ 
HMState HM_Set_Baud(HMBaudRates setbaud ) {
	 char example[100];
	 
	char baud [5];
	
	 strcpy(example, "AT+BAUD");
	
 sprintf(baud,"%d",setbaud);
	strcat(example, baud);
	 

	 


	 
	 		
	
	
   Transmit((example));
	 return state;
}
/**
  * @brief  Connect to the bluetooth address specified
  * @param  address
  * @retval Transmission state
  */ 

HMState HM_Connect (char* address) {
	char command[] = {"AT+CON"};
	strcat(command,address);
	Transmit(command);
	return state;
}

/**
  * @brief  Removal of equipment matching information
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Clear(void) {
	Transmit("AT+CLEAR?");
	return state;
}

/**
  * @brief  Query module working mode
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Mode(void) {
	Transmit("AT+MODE?");
	return state;
}

/**
  * @brief  Set module working mode
  * @param  Mode
  * @retval Transmission state
  */ 
HMState HM_Set_Mode(HMModes mode) {
	 char* msg = "AT + MODE";
	 sprintf(msg, "%d", mode);
   Transmit((msg));
	 return state;
}

/**
  * @brief  Query device name
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Name(void) {
	Transmit("AT+NAME?");
		return state;
}

/**
  * @brief  Set the device name
  * @param  Name
  * @retval Transmission state
  */ 
HMState HM_Set_Name(char* name){
	char Name[] = {"AT+NAME"};
	strcat(Name,name);
	
	Transmit(Name);
	return state;
}

/**
  * @brief  Query pairing password.
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Pass(void){
	Transmit("AT + PASS?");
	return state;
}

/**
  * @brief  Set pairing password.
  * @param  Password
  * @retval Transmission state
  */ 
HMState HM_Set_Password(char* password){
	
	char command[] = {"AT + PASS"};
	strcat(command,password);
	
	Transmit(command);
	return state;
}

/**
  * @brief  Restore factory default settings.
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Renew(void) {
	Transmit("AT+RENEW");
	return state;
}

/**
  * @brief  Reset the module..
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Reset(void) {
	Transmit("AT + RESET");
	return state;
}

/**
  * @brief  Set the master-slave mode
  * @param  Role
  * @retval Transmission state
  */ 
HMState HM_Set_Role(HMRoles role) {
	char* msg = "AT + ROLE";
	 sprintf(msg, "%d", role);
	Transmit((msg));
	 return state;
}

/**
  * @brief  Set the device in sleep mode
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Sleep(void) {
	Transmit("AT+SLEEP");
	return state;
}







/* @brief  Get the device in the work type.
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Get_Work_Type(void){
	Transmit("AT+IMME?");
	return state;
}





/* @brief  Set the device in the desired work type.
  * @param  Work Type
  * @retval Transmission state
  */ 
HMState HM_Set_WorkType(HMWorkType worktype) {
	 char* msg = "AT+IMME";
	 sprintf(msg, "%d", worktype);
   Transmit((msg));
	 return state;
}

/**
  * @brief  Discovers devices.
  * @param  None
  * @retval Transmission state
  */ 
HMState HM_Discover_Devices(void) {
		Transmit("AT+DISC");
	return state;
}
/**
  * @brief  Transmits a command to the BLE sensor..
  * @param  The message
  * @retval Transmission state
  */ 
HMState HM_Transmit(char* data) {
	
	Transmit(data);
	return state;
}
/**
  * @brief  Transmits a command to the BLE sensor..
  * @param  The message
  * @retval None
  */ 
void Transmit(char raw_msg[]) {
		 if (HAL_UART_Transmit(&huart3,(uint8_t*)raw_msg , strlen(raw_msg), HM_TIMEOUT) == HAL_OK) {
			while (HAL_UART_GetState(&huart3) != HAL_UART_STATE_READY){}; 
					state = HM_OK;
					printf("Transmitted: %s\r\n",raw_msg);
}
	 else {
		state = HM_NOK;
	 }
}
