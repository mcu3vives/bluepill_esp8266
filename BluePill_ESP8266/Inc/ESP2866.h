/*
	http://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/#AT
*/

#include "stm32f1xx_hal.h"
#include <stdbool.h>

extern bool multiple_connections;
extern uint8_t rx_data;
extern UART_HandleTypeDef huart2;

/**
	*-> possible ESP states
*/
typedef enum
{
	ESP_OK = 1,
	ESP_NOK = 0
}ESP_STATE;

/** 
	*AT Command 
	*-> Check if module connected
*/
ESP_STATE moduleAvailable(const int timeout, bool debug);

/**
	*AT+RST Command 
	*-> Restarts the module
*/
ESP_STATE restartModule(const int timeout, bool debug);

/** 
	*AT+GMR Command 
	*-> Print firmware version
*/
ESP_STATE fetchFirmwareVersion(const int timeout, bool debug);

/** 
	*AT+GSLP Command 
	*-> Set the time to wait before deep sleep
*/
ESP_STATE enterDeepsleep(int time, const int timeout, bool debug);

/** 
	*ATEx Command 
	*-> Don't send back received command / Send back received command
*/
ESP_STATE toggleEcho(int mode, const int timeout, bool debug);

/** 
	*AT+CWMODE Command 
	*---	Sets the wifi mode ---
	*-> 1 = Station mode (client)
	*-> 2 = AP mode (host)
	*-> 3 = AP + Station mode (Yes, ESP8266 has a dual mode!)
*/
ESP_STATE changeWifiMode(int mode, const int timeout, bool debug);

/** 
	*AT+CWMODE? Command 
	*-> List current wifi mode
*/
ESP_STATE currentWifiMode(const int timeout, bool debug);

/**
	*AT+CWJAP Command 
	*--- Connect to an AP ---
	*-> ssid：String, AP’s SSID
	*-> pwd：String, not longer than 64 characters
*/
ESP_STATE connectToAccessPoint(char* ssid, char* pwd, const int timeout, bool debug);

/** 
	*AT+CWJAP? Command 
	*-> List currently connected network
*/
ESP_STATE currentConnectedAccessPoint(const int timeout, bool debug);

/** 
	*AT+CWLAP Command 
	*--- List all available APs ---
	*-> ecn:
		*-> 0 = OPEN
		*-> 1 = WEP
		*-> 2 = WPA_PSK
		*-> 3 = WPA2_PSK
		*-> 4 = WPA_WPA2_PSK
*-> ssid: String, SSID of AP
*-> rssi: signal strength
*-> mac: String, MAC address
*/
ESP_STATE listAvailableAccessPoints(int ecn, char* ssid, int rssi, char* mac, const int timeout, bool debug);

/** 
	*AT+CWQAP Command 
	*-> Disconnect from AP
*/
ESP_STATE disconnectFromAccessPoint(const int timeout, bool debug);

/** 
	*AT+CWSAP Command 
	*--- Configuration softAP mode ---
	*-> ssid: String, ESP8266’s softAP SSID
	*-> pwd: String, Password, no longer than 64 characters
	*-> ch: channel id
	*-> ecn:
		*-> 0 = OPEN
		*-> 2 = WPA_PSK
		*-> 3 = WPA2_PSK
		*-> 4 = WPA_WPA2_PSK
*/
ESP_STATE configureSoftAPMode(char* ssid, char* pwd, int ch, int ecn, const int timeout, bool debug);

/**
	*-> current softAP mode.
*/
ESP_STATE currentSoftAPMode(const int timeout, bool debug);

/**
	*AT+CWLIF Command 
	*-> List connected clients to softAP
*/
ESP_STATE listConnectedClients(const int timeout, bool debug);

/** 
	*AT+CWDHCP Command 
*--- Enable or Disable DHCP ---
	*-> mode:
		*-> 0 : set ESP8266 as a softAP
		*-> 1 : set ESP8266 as a station
		*-> 2 : set both ESP8266 to both softAP and a station
	*-> en:
		*-> 0 : Enable DHCP
		*-> 1 : Disable DHCP
*/
ESP_STATE toggleDHCP(int mode, int en, const int timeout, bool debug);

/** 
	*AT+CIPSTAMAC Command
	*--- Set MAC address of softAP ---	
	*-> mac： String, MAC address of the ESP8266 softAP.
*/
ESP_STATE setMACAddressStation(char* mac, const int timeout, bool debug);

/** 
	*AT+CIPSTAMAC? Command 
	*-> return current MAC of station
*/
ESP_STATE currentMACAddressStation(const int timeout, bool debug);

/** 
	*AT+CIPAPMAC Command 
	*--- Set MAC of SoftAP ---
	*-> mac： String, MAC address of the ESP8266 softAP.
*/
ESP_STATE setMACAddressSoftAP(char* mac, const int timeout, bool debug);

/** 
	*-> current MAC softAP.
*/
ESP_STATE currentMACAddressSoftAP(const int timeout, bool debug);

/** 
	*AT+CIPSTA Command 
	*--- Set IP of station ---
	*-> ip： String, ip address of the ESP8266 station.
*/
ESP_STATE setIPAddressStation(char* ip, const int timeout, bool debug);

/** 
	*-> current IP Address station.
*/
ESP_STATE currentIPAddressStation(const int timeout, bool debug);
	
/** 
	*AT+CIPAP Command 
	*--- Set IP of softAP ---
	*-> ip： String, ip address of ESP8266 softAP.
*/
ESP_STATE setIPAddressSoftAP(char* ip, const int timeout, bool debug);

/** 
	*-> current IP softAP.
*/
ESP_STATE currentIPAddressSoftAP(const int timeout, bool debug);

/** 
	*AT+CIPSTATUS Command 
	*--- Information about connection ---
	*-> status：
		*-> 2: Got IP
		*-> 3: Connected
		*-> 4: Disconnected
	*-> id： id of the connection (0~4), for multi-connect
	*-> type： String, “TCP” or “UDP”
	*-> addr： String, IP address.
	*-> port： port number
	*-> tetype：
		*-> 0 = ESP8266 runs as a client
		*-> 1 = ESP8266 runs as a server
*/
ESP_STATE connectionInformation(int status, int id, char* type, char* addr, int port, int tetype, const int timeout, bool debug);

ESP_STATE connectionInformationSimple( const int timeout, bool debug);
/** 
	*AT+CIPSTART Command 
	*---  Establish TCP or UDP connection ---
	*-> id: 0-4, id of connection
	*-> type: String, “TCP” or “UDP”
	*-> addr: String, remote IP
	*-> port: String, remote port
*/
ESP_STATE openConnection(int id, char* type, char* addr, int port, const int timeout, bool debug);

/** 
	*-> test connection.
*/
ESP_STATE testConnection(const int timeout, bool debug);

/** 
	*AT+CIPSEND Command 
	*--- Send Data ---
	*-> id: ID no. of transmit connection
	*-> length: data length, MAX 2048 bytes
	*!!! 
		*Wrap return “>” after execute command. 
		*Enters unvarnished transmission, 20ms interval between each packet, 
		*maximum 2048 bytes per packet. 
		*When single packet containing “+++” is received, it returns to command mode.
*!!!
*/
ESP_STATE initSendData(int id, int length, const int timeout, bool debug);

/** 
	*-> test before sending data.
*/
ESP_STATE testSendData(const int timeout, bool debug);

/** 
	*AT+CIPCLOSE Command 
	*--- Close TCP or UDP connection ---
	*-> id： ID no. of connection to close, when id=5, all connections will be closed.
*/
ESP_STATE closeConnection(int id, const int timeout, bool debug);

/** 
	*AT+CIFSR Command 
	*-> Get local IP
*/
ESP_STATE getLocalIPAddress(const int timeout, bool debug);

/** 
	*AT+CIPMUX Command 
	*--- Use or don't use multiple connections ---
	*-> mode:
		*-> 0: Single connection
		*-> 1: Multiple connections (MAX 4)
*/
ESP_STATE toggleMultipleConnection(int mode, const int timeout, bool debug);

/** 
	*AT+CIPMUX. Command 
	*-> current multiplex mode
*/
ESP_STATE currentMultipleConnectionMode(const int timeout, bool debug);

/** 
	*AT+CIPSERVER Command 
	*--- Configure ESP2866 as 'SERVER' ---
	*-> mode:
		*-> 0: Delete server (need to follow by restart)
		*-> 1:	Create server
	*-> port: port number, default is 333

*/
ESP_STATE configureAsServer(int mode, int port, const int timeout, bool debug);

/**
	*AT+CIPMODE Command 
	*--- Set transfer mode ---
	*-> mode:
		*-> 0: normal mode
		*-> 1: unvarnished transmission mode
*/
ESP_STATE setTransferMode(int mode, const int timeout, bool debug);

/**
	*-> get current transfer mode
*/
ESP_STATE currentTransferMode(const int timeout, bool debug);
/** 
	*AT+CIPSTO Command 
	*--- Set server timeout ---
	*-> time: server timeout, range 0~7200 seconds
*/
ESP_STATE setServerTimeout(int time, const int timeout, bool debug);

/** 
	*-> get current server timeout.
*/
ESP_STATE currentServerTimeout(const int timeout, bool debug);

/** 
	*-> request data from ESP2866 device over UART
*/
char readFromESP(void);

/** 
	*-> request data from ESP2866 device over UART and convert to C-String
*/
char* readFromESPAsString(int timeout, bool debug);

/** 
	*-> Handle received data from ESP
*/
ESP_STATE receiveHandling(int timeout, bool debug);

/** 
	*-> Handle received data from ESP used in Server mode to read received messages
*/
char* receiveHandlingAlt(int timeout, bool debug);

/** 
	*-> Manually send data over UART
*/
void sendData(char* msg);




