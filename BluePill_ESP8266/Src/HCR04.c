#include "HCR04.h"
#include "stm32f1xx_hal.h"
#include <stdlib.h>
#include <stdio.h>


void triggerSensor (void) {
HAL_GPIO_WritePin(HCSR04_Trigger_GPIO_Port,HCSR04_Trigger_Pin,GPIO_PIN_RESET); 
	HAL_Delay(1); 
HAL_GPIO_WritePin(HCSR04_Trigger_GPIO_Port,HCSR04_Trigger_Pin,GPIO_PIN_SET); 
	HAL_Delay(1); 
HAL_GPIO_WritePin(HCSR04_Trigger_GPIO_Port,HCSR04_Trigger_Pin,GPIO_PIN_RESET); 
	}

long echoSensor(void) {
	
		
//	duration = (HAL_GPIO_ReadPin(Echo_GPIO_Port,Echo_Pin) == GPIO_PIN_SET); 
	 uint8_t flag=0;
   uint32_t disTime=0;
	
	 while(flag == 0)
  {
	while	(HAL_GPIO_ReadPin(HCSR04_Echo_GPIO_Port,HCSR04_Echo_Pin) == GPIO_PIN_SET)
		{
			disTime++;
     flag = 1;
		}
	}
		
	
	return disTime; 
}

long microsecondsToCentimeters(long microseconds)
{

  return microseconds / 29 / 2;
}


HCR04State HCR04_Set_Distance(int triggerdistance) {
	
	triggerSensor();
	
		long rawdistance = echoSensor(); 
	long distance = microsecondsToCentimeters(rawdistance); 
	
		if (distance <= 100) {
		//printf("%ld \r\n",distance); 
		HAL_Delay(10); 
		
		if (distance <= triggerdistance) {
			//HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_SET); 
			return Ok;
		}
		else {
			//HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_RESET); 
			return NOk;
		}
	}
	
	return NOk;
	}
