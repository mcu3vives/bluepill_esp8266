#include "circularBufferHandling.h"

CIRCBUF_DEF(myDatBuf, SERIAL_RX_BUFFER_SIZE);
circBuf_t *cb = &myDatBuf;

int circBufPush(uint8_t data)
{
    int count = (cb->head + 1) % SERIAL_RX_BUFFER_SIZE; // count is where head will point to after this write.

    if (count == cb->tail) // check if circular buffer if full.
        return -1; // if so return with an error.
		
		
    cb->buffer[cb->head] = data; // Load data and then move
    cb->head = count; // head to next data offset.

    return 0; // return success to indicate a successful push.
}

int circBuffPop(uint8_t *data)
{
    // if the head isn't ahead of the tail, we don't have any characters.
    if (cb->head == cb->tail)  // check if circular buffer is empty.
        return -1; // if so return with an error.

    int count = (cb->tail + 1) % SERIAL_RX_BUFFER_SIZE; // next is where tail will point to after this read.

    *data = cb->buffer[cb->tail]; // read data and then move.
    cb->tail = count; // tail to next data offset.
    return 0; // return success to indicate successful pop.
}

int circAvailable()
{
	
	return ((unsigned int)(SERIAL_RX_BUFFER_SIZE + cb->head - cb->tail)) % SERIAL_RX_BUFFER_SIZE;
}

void circFlush(){
	while((cb->head != cb->tail)) {
			__NOP();
		}
}
