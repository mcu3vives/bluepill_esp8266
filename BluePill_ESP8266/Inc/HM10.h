 #include "stm32f1xx_hal.h"

#define HM_TIMEOUT 5000
extern UART_HandleTypeDef huart3;
 
 typedef enum  {
	 HM_OK = 0,
	 HM_NOK = -1
 }HMState; 
 
 typedef enum {
	rate0 = 0,
	rate1 = 1,
	rate2 = 2,
	rate3 = 3,
	rate4 = 4,
	rate5 = 5,
	rate6 = 6,
  rate7 = 7,
  rate8 = 8
 }HMBaudRates;
 
 typedef enum {
	 Transmission_Mode = 0,
	 PIO_Collection_Mode0 = 1,
	 Remote_Control_Mode0 = 2
 }HMModes;
 
 typedef enum {
	 Peripheral = 0,
	 Central = 1
 }HMRoles;
 
 typedef enum {
	 
	 Standard = 0,
	 Automatic_Connection = 1
	 
 }HMWorkType;
 
HMState HM_Get_Adress(void);
HMState HM_Get_Baud(void);
HMState HM_Set_Baud(HMBaudRates setbaud);
HMState HM_Connect(char* address);
HMState HM_Transmit(char* data);
HMState HM_Clear(void);
HMState HM_Get_Mode(void);
HMState HM_Set_Mode(HMModes mode);
HMState HM_Get_Name(void);
HMState HM_Set_Name(char* name);
HMState HM_Get_Pass(void);
HMState HM_Set_Password(char* password);
HMState HM_Renew(void);
HMState HM_Reset(void);
HMState HM_Set_Role(HMRoles role);
HMState HM_Sleep(void);
HMState HM_Get_Work_Type(void);
HMState HM_Set_WorkType(HMWorkType worktype);
